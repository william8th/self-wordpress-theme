<div id="middle-row" class="row">
	<div id="home" class="container">

	
	<?php
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$wp_query = new WP_Query( array( 'posts_per_page' => 6, 'paged' => $paged ) );

	if ( have_posts() ) :
		$count = 0;
		$post_count = $wp_query->post_count;

		while( have_posts() ) : the_post();
			$count++;

			/* Tabulate the rows */
			if( $count == 1 || $count == 4 ) echo '<div class="row pulldown50">';

			/* Tabulate the columns */
			if( $count == 1 || $count == 4 ) echo '<div class="span4">';
			if( $count == 2 || $count == 5 ) echo '<div class="span4">';
			if( $count == 3 || $count == 6 ) echo '<div class="span4">';
	?>

				<div id="home-entry-image">
					<a href="<?php the_permalink(); ?>">
						<?php if( has_post_thumbnail() ) : the_post_thumbnail( 'self-thumbnail' ); ?>
						<?php else : print_no_image(); ?>
						<?php endif; ?>
					</a>
				</div> <!-- home-entry-image -->

				<div id="home-entry-title">
					<a href="<?php the_permalink(); ?>" rel="post title" title="<?php the_title(); ?>">
						<h1>
							<?php the_title(); ?>
						</h1>
					</a>
				</div> <!-- home-entry-title -->

				<div id="home-entry-content" title="<?php the_title(); ?>">
					<?php print_content( get_the_content() ); ?>
				</div> <!-- home-entry-content -->

	<?php
				echo '</div> <!-- span4 -->';
				if( $count == 3 || $count == 6 ) echo '</div> <!-- row -->';

				//End the div if post count is not of the multiples of 3
				if( $post_count % 3 != 0 && $count == $post_count ) echo '</div> <!-- home-row -->';

		endwhile;
	endif;
	?>

	<?php /* The pagination navigation bar */ ?>
	<?php if (  $wp_query->max_num_pages > 1 ) : ?>
		<div class="row pulldown30">
			<div class="nav-below span12">
				<div class="pull-left">
					<?php
						$previous = get_previous_posts_link();
						if( ! empty( $previous ) )
							previous_posts_link( '<< Previous' );
					?>
				</div> <!-- pull-left -->

				<div class="pull-right">
					<?php 
						$next = get_next_posts_link();
						if( ! empty( $next ) )
							next_posts_link( 'Next >>' );
					?>
				</div> <!-- pull-left -->

			</div><!-- nav-below -->
		</div> <!-- row -->
	<?php endif; ?>

	</div> <!-- home -->
</div> <!-- middle-row -->