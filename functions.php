<?php

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 300, 150, true );
	add_image_size( 'self-thumbnail', 300, 150, true );
}

function print_content( $content ) {
	echo esc_html( substr( strip_tags( $content ), 0, 250 ) ) . '...';
}

function print_no_image() {
	echo '<img src="' . get_bloginfo( 'template_url' ).'/assets/img/no-image.jpg" />';
}

function print_timestamp( $date, $time ) {
	esc_html_e( $date . '&nbsp;' . $time );
}

add_filter( 'next_posts_link_attributes', 'posts_link_attributes' );
add_filter( 'previous_posts_link_attributes', 'posts_link_attributes' );

function posts_link_attributes() {
    return 'class="btn"';
}

function disqus_embed($disqus_shortname) {
    global $post;
    wp_enqueue_script('disqus_embed','http://'.$disqus_shortname.'.disqus.com/embed.js');
    echo '<div id="disqus_thread" class="span10 offset1"></div>
    <script type="text/javascript">
        var disqus_shortname = "'.$disqus_shortname.'";
        var disqus_title = "'.$post->post_title.'";
        var disqus_url = "'.get_permalink($post->ID).'";
        var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
    </script>';
}

?>