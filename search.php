<?php
/*
Template Name: Search Page
*/
get_header();
?>

<?php
global $query_string;

$query_args = explode( "&", $query_string );
$search_query = array();

foreach( $query_args as $key => $string ) {
	$query_split = explode( "=", $string );
	$search_query[$query_split[0]] = urldecode( $query_split[1] );
}

$search = new WP_Query( $search_query );

?>

	<div id="middle-row" class="row">
		<div id="search-header" class="span12 pulldown50">
			<h1>
				<?php
					if( is_search() )
						printf( __( 'Search results for: %s' ), '<span>' . get_search_query() . '</span>' );
					elseif( is_tag() )
						printf( __( 'Tag archives: %s' ), '<span>' . get_query_var( 'tag' ) . '</span>' );
				?>
			</h1>
		</div> <!-- search-header -->

		<?php if ( have_posts() ) : ?>

		<div id="search-entries" class="container">
			<?php
			if( is_search() || is_tag() ) :
				while( have_posts() ) : the_post();
			?>
				<div id="search-entry" class="row pulldown50">
					<div id="search-entry-title" class="span12">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<h1>
								<?php the_title(); ?>
							</h1>
						</a>
					</div> <!-- search-entry-title -->
					
					<div id="search-entry-content" class="span12">
						<?php
							$summary = get_the_excerpt();
							echo '<p>' . $summary . '</p>';
						?>
					</div> <!-- search-entry-content -->
				</div> <!-- search-entry -->
			<?php
				endwhile;
			endif;
			?>
		</div> <!-- search-entries -->

		<?php else : ?>
			<div id="not-found" class="row">
				<div class="span10 well pulldown30">
					<?php _e( 'Sorry, but nothing matched your search criteria. Please refine your keywords.' ); ?>
				</div>
			</div> <!-- not-found -->

		<?php endif; ?>
	</div> <!-- middle-row -->
<?php get_footer(); ?>