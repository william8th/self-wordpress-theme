<?php get_header(); ?>

<div id="middle-row" class="row">
	<div class="container">
		<div id="entry" class="row">
			<?php
			if( have_posts() && is_singular() ) :
				while( have_posts() ) : the_post();
			?>
			<article class="span10 offset1 pulldown50">
				<div id="entry-title" class="aligncenter">
					<h1>
						<?php the_title(); ?>
					</h1>
				</div> <!-- entry-title -->

				<div id="entry-timestamp" class="aligncenter">
					<strong>
						<?php print_timestamp( get_the_date(), get_the_time() ); ?>
					</strong>
				</div> <!-- entry-timestamp -->

				<div id="entry-content" class="pulldown50">
					<?php the_content(); ?>
				</div> <!-- entry-content -->

				<div id="entry-author" class="pulldown50">
					<h6>
						Written by <?php the_author(); ?>
					</h6>
				</div> <!-- entry-author -->

				<?php the_tags( '<div class="entry-tags">Tags: ', ' , ', '</div> <!-- entry-tags -->' ); ?>
			</article>
			<?php
				endwhile;
			endif;
			?>
			<?php disqus_embed( '' ); ?>
		</div> <!-- entry -->

		<div id="social" class="row">
			<div class="offset1 span10 pulldown20">
				<span class='st_facebook_hcount' displayText='Facebook'></span>
				<span class='st_twitter_hcount' displayText='Tweet'></span>
				<span class='st_plusone_hcount' displayText='Google +1'></span>
			</div> <!-- span10 -->
		</div> <!-- social -->	

		<div id="post-navigation" class="row pulldown50">
			<div class="offset1 span3 text-left">
				<?php previous_post_link(); ?>
				&nbsp;&nbsp;
			</div>
			
			<div class="offset1 span2 text-center">
				<a href="<?php bloginfo( 'url' ); ?>">
					Back to blog
				</a>
			</div>

			<div class="offset1 span3 text-right">
				&nbsp;&nbsp;
				<?php next_post_link(); ?>
			</div>
		</div> <!-- post-navigation -->
	</div> <!-- container -->
</div> <!-- middle-row -->






<?php get_footer(); ?>