Self Theme for WordPress
========================

'Self' is a theme for the WordPress CMS platform.

This theme is aimed to display a thumbnail (300px x 150px) for each post, creating a zine-like interface with a featured image to portray and emphasize on the theme of each individual post. The main design of the theme is focused on a clean white background with a light and minimalistic web font while accentuating the title of the post in a crude, bold black to create a comfortable reading experience.

License
-------
This theme is under the GPLv2 (GNU General Public License) license.


**Copyright (C) 2012 William Heng**

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

**Fonts and stylesheets used are of external sources and they are each subject to inherited copyright restrictions.**

Installation
------------
This is a theme created for the WordPress CMS platform. If there are any questions regarding the use of WordPress, please visit [Wordpress.org](http://wordpress.org) for more information.

Download the .zip file and extract it into the WordPress theme's folder, e.g. *wordpress/wp-content/themes*. Rename the folder that is extracted as 'self'. The location of the 'self' folder should be in e.g. *wordpress/wp-content/themes/self*.

**IMPORTANT NOTE**: Go to *Settings->Reading* and change the **'Blog pages show at most'** to **6**. This is important for the pagination to work.

Additional details
------------------
Comments section will be added in the coming months.