<div id="top-row" class="row">
	<div id="top-menu" class="container menu">
		<div id="navbar-row" class="row">
			<div id="blog-essentials" class="span8">
				<div id="blog-title">
					<a href="<?php bloginfo( 'url' ); ?>" rel="index">
						<h1 title="<?php bloginfo('title'); ?>">
							<?php bloginfo( 'name' ); ?>
						</h1>
					</a>
				</div> <!-- blog-title -->
			
				<div id="blog-description" title="Description">
					<h6>
						<?php bloginfo( 'description' ); ?>
					</h6>
				</div> <!-- blog-description-->

				<div id="blog-pages">
					<ul>
						<?php
							$mypages = get_pages( array( 'sort_column' => 'post_date', 'sort_order' => 'desc' ) );

							foreach( $mypages as $page ) {
								$content = $page->post_content;
								if ( ! $content )
									continue;

								$content = apply_filters( 'the_content', $content );
						?>
								<li>
									<a href="<?php echo get_page_link( $page->ID ); ?>">
										<?php echo $page->post_title; ?>
									</a>
								</li>
						<?php
							}
						?>
					</ul>
				</div> <!-- blog-pages -->
			</div> <!-- blog-essentials -->

			<div id="search-bar" class="span4">
				<div class="span4 pull-right pulldown20">
					<?php get_search_form(); ?>
				</div> <!-- span4 -->
			</div><!-- search-bar -->

		</div> <!-- navbar-row -->
	</div> <!-- top-menu -->
</div> <!-- top-row -->