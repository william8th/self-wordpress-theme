<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<title>
			<?php
				/*
				 * Print the <title> tag based on what is being viewed.
				 * Make the theme more SEO-friendly.
				 */
				global $page, $paged;

				// Add the blog name.
				bloginfo( 'name' );

				wp_title( '-', true, 'left' );

				// Add the blog description for the home/front page.
				$site_description = get_bloginfo( 'description', 'display' );
				if ( $site_description && ( is_home() || is_front_page() ) )
					echo " - $site_description";

				// Add a page number if necessary:
				if ( $paged >= 2 || $page >= 2 )
					echo ' - ' . sprintf( __( 'Page %s' ), max( $paged, $page ) );

			?>
		</title>

		<!-- Twitter Bootstrap -->
		<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/bootstrap/css/bootstrap.min.css">
		
		<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

		<?php wp_head(); ?>
	</head>
	<body>

	<div id="wrap" class="container">

	<?php get_template_part( 'navbar' ) ?>