<?php get_header(); ?>

<div id="middle-row" class="row">
	<div class="well pulldown50">
		<h1>Page not found</h1>
		<p>I'm sorry but the page you are trying to reach is not found.</p>
	</div>
</div> <!-- middle-row -->

<?php get_footer(); ?>