<form class="search pull-right" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
	<input type="text" class="search-query" placeholder="Search" name="s" id="s">
</form>