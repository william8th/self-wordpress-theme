			<div id="bottom-row" class="row pulldown50">
				<div id="footer" class="pulldown20">
					<div id="bloginfo-rss" class="span4 text-left">
						<a href="<?php bloginfo( 'url' ); ?>" rel="index" title="<?php bloginfo( 'name' ) ?>">
							<i class="icon-home"></i><?php bloginfo( 'name' ); ?>
						</a>
						<br />
						<a href="<?php bloginfo( 'rss2_url' ); ?>" rel="rss" title="RSS feed">
							<i class="icon-bookmark"></i>RSS
						</a>
					</div> <!-- bloginfo-rss -->

					<div id="copyright" class="span4 text-center">
						Copyright &copy; <?php echo date( 'Y' ); bloginfo( 'name' ); ?>
					</div> <!-- copyright -->

					<?php
					/*
					*	Give Credit Where Credit Is Due
					*	Visit http://is.gd/gcwcid before deciding to delete the credits section.
					*/
					?>
					<div id="credits" class="span4 pull-right text-right">
						Proudly powered by <a href="http://wordpress.org/" title="WordPress">WordPress</a>
						<br />
						Self theme by <a href="http://williamheng.com/" title="William Heng">William Heng</a>
					</div> <!-- credits -->
				</div> <!-- footer -->
			</div> <!-- bottom-row -->

		</div> <!-- wrap -->

		<?php /* Javascripts are placed at the bottom to speed up the loading of the page */ ?>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">stLight.options({publisher: "ur-fc9be1c6-7d1d-68bb-7d5a-2742f175b9b0"}); </script>

		<?php wp_footer(); ?>
	</body>
</html>